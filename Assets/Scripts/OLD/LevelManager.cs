﻿using UnityEngine;
using System.Collections;

public class LevelManager : MonoBehaviour {

	public scroll ScrollFondo, ScrollSegPlano, ScrollPriPlano;

	private float speed = 0.0005f;

	void Awake() {}

	void Update() {
		ScrollFondo.SetSpeed(speed);
		ScrollSegPlano.SetSpeed(speed);
		ScrollPriPlano.SetSpeed(speed);
	}


}
