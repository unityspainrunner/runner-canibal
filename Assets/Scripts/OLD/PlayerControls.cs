﻿using UnityEngine;
using System.Collections;

public class PlayerControls : MonoBehaviour {

	private Animator myAnimator;


	[Range(1f,20f)]
	public float JumpHeight=5f;
	public float dashingTime = 1F;
	public bool grounded;
	private Vector3 CurrentVel;
	
	private bool dashing;
	private float lastDashingTime;
	
	private Vector2 originalColliderSize;
	private BoxCollider2D collider;

	void Awake() {
		myAnimator = GetComponentInChildren<Animator>();
		grounded = false;
	}
	
	void Start()
	{
		collider = GetComponent<BoxCollider2D>();
		originalColliderSize = collider.size;
	}

	void Update () {
	
		Vector2 pos = transform.position, scale = transform.localScale;
		
		if ( Input.GetKeyDown(KeyCode.W) && grounded && !dashing )		// Salto
			Action_Salta();
		if ( Input.GetKeyDown(KeyCode.S) && grounded && !dashing ) {	// Dash
			Action_Dash();
		}
		
		if ( (Time.time - lastDashingTime) > dashingTime ) {
			dashing = false;
			collider.size = originalColliderSize;
		}
		
		
		// compribar si está "pisando" algo.
		RaycastHit2D hit = Physics2D.Raycast( new Vector2(pos.x + scale.x/2, pos.y - scale.y/2), -Vector3.up );
		grounded = hit != null && hit.distance <= 0.01f;
		
		if ( grounded ) {
			if ( !dashing )
				myAnimator.Play( Animator.StringToHash( "player_run" ) );
			else
				myAnimator.Play( Animator.StringToHash( "player_dash" ) );
		}
		else
			myAnimator.Play( Animator.StringToHash( "player_jump" ) );
	}
	
	//metodo de boton que salta el personaje
	public void Action_Salta() {
		GetComponent<Rigidbody2D>().AddForce( Vector2.up * JumpHeight, ForceMode2D.Impulse );
		grounded = false;
	}

	//metodo de boton que desliza el personaje
	public void Action_Dash() {
		collider.size = new  Vector2( collider.size.x, collider.size.y / 2F );
		lastDashingTime = Time.time;
		dashing = true;
	}

	void OnTriggerEnter2D( Collider2D col )
	{
		if ( col.gameObject.tag == "insta-kill" ) {
			PlayerData.Vidas--;
			Debug.Log (" M.U.E.R.T.O. ");
		}
	}

}
